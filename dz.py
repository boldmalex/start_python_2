constHelp = """
help - напечатать справку по программе
add - добавить задачу в список дел (название запрашивается у пользователя)
show - вывести список текущи задач
"""

constToday = 'сегодня'
constTomorrow = 'завтра'
constOther = 'другие'

todayTasks = []
tomorrowTasks = []
otherTasks = []


isRun = True

while isRun:

    command = input("Введите команду:")

    if (command == "help"):
        print(constHelp)
    elif (command == "add"):
        task = input("Введите задачу: ")
        date = input("Введите дату (сегодня, завтра или другое): ")
        if (date == constToday):
            todayTasks.append(task)
        elif (date == constTomorrow):
            tomorrowTasks.append(task)
        else:
            otherTasks.append(task)
        print(task, " - задача добавлена")
    elif (command == "show"):
        print(constToday, ": ", todayTasks, constTomorrow,
              ": ", tomorrowTasks, constOther, ": ", otherTasks)
    elif (command == "exit"):
        print("Спасибо за использование! До свидания!")
        isRun = False
    else:
        print("Команда не определена, воспользуйтесь командой 'help'")
